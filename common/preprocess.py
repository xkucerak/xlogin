from sys import argv
from glob import glob
from os import path
from typing import Optional
import re

TAGS = {
    '#root': [('kocicky', '/kocicky/'), ('lowlevel', '/lowlevel/'), ('algo', '/algo/'), ('bell', '/bell/')],
    '#kocicky': [('kocicky', '/kocicky/'), ('cvičenia', '/kocicky/cvicenia/'), ('xkucerak', '/')],
    '#typci': [('typci', '/typci/'), ('xkucerak', '/')],
    '#lowlevel': [('lowlevel', '/lowlevel/'), ('xkucerak', '/')],
    '#algo': [('algo', '/algo/'), ('xkucerak', '/')],
    '#bell': [('bell', '/bell/'), ('xkucerak', '/')],
}

DESIGN = {
    'root':
"""
```
 __          __          __
/ž \\_____   /j \\_____   /b \\_____
\\__/-="="`  \\__/-="="`  \\__/-="="`
```
""",
    '':
"""
    'typci-old':
```
       (⌐■_■)(⌐■_■)
  (⌐■_■)          (⌐■_■)
(⌐■_■)              (⌐■_■)
(⌐■_■)              (⌐■_■)
  (⌐■_■)          (⌐■_■)
       (⌐■_■)(⌐■_■)

```
""",
    'typci':
"""
```
 ,_     _
 |\\_,-~/
 /      |    ,--.
▝▀▜█▛▜█▛▀   / ,-'
 \\  _T_/-._( (
 /         `. \\
|         _  \\ |
 \\ \\ ,  /      |
  || |-_\\__   /
 ((_/`(____,-' λ
```
""",
    'kocicky':
"""\
```
  |\\      _,,,---,,_
  /,`.-'`'    -.  ;-;;,_
 |,4-  ) )-,_..;\\ (  `'-'
'---''(_/--'  `-'\\_)
```
""",
    'lowlevel':
"""\
```
 ,adPPYba,
a8"     ""
8b
"8a,   ,aa
 `"Ybbd8"'
```
""",
    'algo':
"""
```
        ;     /        ,--.
       ["]   ["]  ,<  |__**|
      /[_]\\  [~]\\/    |//  |
       ] [   OOO      /o|__|   Finding Karel
```
""",
    'bell':
"""
```
   _
  /\\`--.
 |o-|   )>=====o
  \\/.--'
```
"""
}

PRESETS = {
    'kocicky':
"""\
maintitle: Seminár teórie kategórii
pagetitle: Seminár teórie kategórii
header: kocicky
nav: #kocicky
""",
    'typci':
"""\
maintitle: Seminár teórie typov
pagetitle: Seminár teórie typov
header: typci
nav: #typci
""",
    'lowlevel':
"""\
maintitle: Coočerák's Cringe C Code Corner
pagetitle: Coočerák's Cringe C Code Corner
header: lowlevel
nav: #lowlevel
""",
    'algo':
"""\
maintitle: Algoritmy
pagetitle: Algoritmy
header: algo
nav: #algo
""",
    'root':
"""\
nav: #root
header: root
""",
    'bell':
"""\
maintitle: Should ring a bell
pagetitle: Should ring a bell
nav: #bell
header: bell
"""
}

def get_posts(relpath):
    posts = []
    for file in glob(f"pages/{relpath}/*.pst", recursive=True):
        posts.append(file)
    return posts

def build_navigation(posts):

    navigation = []
    for post in posts:
        rel_path = path.join(*post.split(path.sep)[1:])
        rel_filename, rel_extenion = path.splitext(rel_path)
        html_path = rel_filename + ".html"
        navigation.append((path.split(rel_filename)[-1], '/' + html_path))
    return navigation

def distinct(lst):
    res = []
    seen = set()
    for e in lst:
        if e in seen:
            res.append(e)
            seen.add(e)
    return res

def parse_navigation(nav):
    m = re.match(r'\[([^\]]+)\]\(([^\)]+)\)', nav)

    name = m.group(1)
    link = m.group(2)
    return name, link

def print_header(state):

    title = 'xkucerak' if state.title is None else state.title
    if state.pagetitle is not None:
        title += ': ' + state.pagetitle

    HEADER = \
f"""\
# {title}
"""

    nav = state.nav if state.nav is not None else ['**']
    navigation = []

    posts = []
    for n in nav:
        if n.startswith('#'):
            navigation.extend(TAGS[n])
        if n.startswith('['):
            navigation.append(parse_navigation(n))

        posts.extend(posts)

    navigation.extend(build_navigation(distinct(posts)))

    print(HEADER)

    if state.header is not None:
        print(DESIGN[state.header.strip()])

    for name, link in navigation:
        real_link = '%ROOT%' + link if link.startswith('/') else link
        print(f'[{name}]({real_link})')

    print()



class State:

    def __init__(self):
        self.nav = None
        self.pagetitle = None
        self.header = None
        self.title = None

def eval_value(state, name, value) -> Optional[str]:
    if name == 'nav':
        state.nav = value.split()
    if name == 'posttitle':
        state.pagetitle = value
        print(f'pagetitle: {value}')
    if name == 'header':
        state.header = value
    if name == 'maintitle':
        state.title = value
    if name == 'preset':
        return PRESETS[value]


if __name__ == "__main__":
    filename = argv[1]

    counter = 0
    state = State()

    with open(filename, 'r') as f:
        header_added = False

        stack = [(f.readlines(), [0])]

        overall_index = 0

        while stack:
            lines, index_ptr = stack[-1]
            index = index_ptr[0]
            if index >= len(lines):
                stack.pop()
                continue

            line = lines[index]
            if not header_added and overall_index == 0 and not line.strip().startswith('---'):
                print_header(state)
                header_added = True
            print(line, end='')
            if not header_added and line.strip().startswith('---'):
                counter += 1
                if counter == 2:
                    print_header(state)
                    header_added = True

            if counter == 1:
                if ':' in line:
                    line_split = line.split(':')
                    if len(line_split) == 2:
                        name, value = line_split
                        res = eval_value(state, name.strip(), value.strip())
                        if res is not None:
                            stack.append(([s + '\n' for s in res.splitlines()], [0]))
            index_ptr[0] += 1
            overall_index += 1
